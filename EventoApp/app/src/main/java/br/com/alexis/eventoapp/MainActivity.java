package br.com.alexis.eventoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String[] arrayAulas = getResources().getStringArray(R.array.array_aulas);

        LinearLayout lista = (LinearLayout) findViewById(R.id.lista);

        for (String aula : arrayAulas){
            TextView textView = new TextView(this);
            textView.setText(aula);

            lista.addView(textView);
        }


    }
}
